module ApplicationHelper
  def flash_class(level)
    case level
      when "notice" then "alert-primary"
      when "success" then "alert-success"
      when "error" then "alert-danger"
      else "alert-warning"
    end
  end

  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end
end
